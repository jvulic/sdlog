package sdlog

import (
	"github.com/Sirupsen/logrus"
	"testing"
)

const projectID = "your-project-id"

func TestBasicLog(t *testing.T) {
	hook, err := NewHook(projectID, "test")
	if err != nil {
		t.Fatalf("Failed to create log hook: %v", err)
	}
	logrus.AddHook(hook)
	logrus.WithField("label", "value").Info("Here is some info!")
}
