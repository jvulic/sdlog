# SDLog

[GoDoc]: https://godoc.org/bitbucket.org/jvulic/sdlog
[GoDoc Widget]: https://godoc.org/bitbucket.org/jvulic/sdlog?status.png
[Go Report Card]: https://goreportcard.com/report/bitbucket.org/jvulic/sdlog
[Go Report Card Widget]: https://goreportcard.com/badge/bitbucket.org/jvulic/sdlog

[![Go Doc][GoDoc Widget]][GoDoc] [![Go Report Card][Go Report Card Widget]][Go Report Card]

Package provides [logrus](https://github.com/Sirupsen/logrus) hooks for [google
stackdriver logging](https://cloud.google.com/logging/).

In particular, it uses the go library provided by [google cloud
go](https://github.com/GoogleCloudPlatform/google-cloud-go)
[docs](https://godoc.org/cloud.google.com/go/logging) to log to Google
Stackdriver Logging. Refer to [Application Default
Credentials](https://cloud.google.com/docs/authentication#getting_credentials_for_server-centric_flow)
for how to perform authorization.

### Usage
```go
hook, err := sdlog.NewHook("my-project-id")
if err != nil {}
logrus.Hooks.Add(hook)
```

## External References
[3-clause BSD]: https://tldrlegal.com/license/bsd-3-clause-license-(revised)
[MIT]:https://tldrlegal.com/license/mit-license
[Apache 2.0]:https://tldrlegal.com/license/apache-license-2.0-(apache-2.0)

[logrus](https://github.com/sirupsen/logrus) [MIT][MIT]

[google cloud](https://github.com/GoogleCloudPlatform/google-cloud-go) [Apache 2.0][Apache 2.0]
