/*
Package sdlog
*/
package sdlog

import (
	"fmt"

	"cloud.google.com/go/logging"
	"github.com/Sirupsen/logrus"
	"golang.org/x/net/context"
)

// Hook ...
type Hook struct {
	// TODO(jv): Add a method to close the client (ensure the log buffer is
	// flushed).
	client *logging.Client
	lg     *logging.Logger
}

// NewHook ...
func NewHook(projectID, serviceName string) (*Hook, error) {
	client, err := logging.NewClient(context.Background(), projectID)
	if err != nil {
		return nil, err
	}
	return &Hook{client: client, lg: client.Logger(serviceName)}, nil
}

// Fire ...
func (hook *Hook) Fire(entry *logrus.Entry) error {
	// convert entry data to labels
	labels := make(map[string]string, len(entry.Data))
	for k, v := range entry.Data {
		switch x := v.(type) {
		case string:
			labels[k] = x
		default:
			labels[k] = fmt.Sprintf("%v", v)
		}
	}

	msg := logging.Entry{
		Labels:  labels,
		Payload: entry.Message,
	}
	switch entry.Level {
	case logrus.PanicLevel:
		msg.Severity = logging.Alert
		hook.lg.LogSync(context.Background(), msg)
	case logrus.FatalLevel:
		msg.Severity = logging.Critical
		hook.lg.LogSync(context.Background(), msg)
	case logrus.ErrorLevel:
		msg.Severity = logging.Error
		hook.lg.LogSync(context.Background(), msg)
	case logrus.WarnLevel:
		msg.Severity = logging.Warning
		hook.lg.LogSync(context.Background(), msg)
	case logrus.InfoLevel:
		msg.Severity = logging.Info
		hook.lg.LogSync(context.Background(), msg)
	case logrus.DebugLevel:
		msg.Severity = logging.Debug
		hook.lg.LogSync(context.Background(), msg)
	default:
		msg.Severity = logging.Default
		hook.lg.LogSync(context.Background(), msg)
	}
	return nil
}

// Levels ...
func (hook *Hook) Levels() []logrus.Level {
	return logrus.AllLevels
}
